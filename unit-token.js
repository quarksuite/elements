import { QSElement } from "./lib.js";

export class UnitToken extends QSElement {
  constructor() {
    super();
  }

  render() {
    const length = this.length || "960px";
    const grid = (this.grid === "" && true) || false;

    return template(length, grid);
  }

  static get observedAttributes() {
    return ["length", "grid"];
  }

  set length(value) {
    this.reflect("length", value);
  }

  get length() {
    return this.getAttribute("length");
  }

  set grid(_) {
    this.reflect("grid", "");
  }

  get grid() {
    return this.getAttribute("grid");
  }
}

if (!customElements.get("unit-token")) {
  customElements.define("unit-token", UnitToken);
}

function template(length, grid) {
  const tmpl = document.createElement("template");

  tmpl.innerHTML = `
${styles(length, grid)}
<div part="element">
  <div></div>
  <div></div>
  <div></div>
</div>
<div part="data">
  <span part="value length">length: <code part="code">"${length}"</code></span>
</div>
`;

  return tmpl.content.cloneNode(true);
}

function styles(length, grid) {
  return `
<style>
  :host {
    --spacing: 1ex;
    --element-gap: var(--spacing);
    --element-height: 2vmin;
    --element-color: currentcolor;
    --length-color: gray;
    --data-size: 1rem;

    display: block;
  }

  :host[hidden] {
    display: none;
  }

  [part="element"] {
    display: grid;
    gap: var(--element-gap);
    grid-template-columns: ${
      grid ? `${length} repeat(2, 1fr)` : "auto repeat(2, 0.25ex)"
    };
    min-height: var(--element-height);
  }

  [part="element"] > div {
    background-color: var(--element-color);
  }

  [part="element"] > div:nth-of-type(1) {
    background-color: var(--length-color);
    max-width: ${length};
  }

  [part="data"] {
    font-size: var(--data-size);
    margin-top: var(--spacing);
  }
</style>
`;
}
