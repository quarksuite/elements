# QuarkSuite Elements (v0.6.0)

## Table of Contents

- [Summary](#summary)
- [Packages](#packages)
- [Getting Started](#getting-started)
- [Elements](#elements)
  - [\<color-token>](#color-token)
    - [Properties/Attributes](#properties-attributes)
    - [Customization](#customization)
    - [Examples](#examples)
  - [\<text-token>](#text-token)
    - [Properties/Attributes](#properties-attributes-1)
    - [Customization](#customization-1)
    - [Examples](#examples-1)
  - [\<unit-token>](#unit-token)
    - [Properties/Attributes](#properties-attributes-2)
    - [Customization](#customization-2)
    - [Examples](#examples-2)
  - [\<grid-token>](#grid-token)
    - [Properties/Attributes](#properties-attributes-3)
    - [Customization](#customization-3)
    - [Examples](#examples-3)

## Summary

QuarkSuite Elements is a library of vanilla web components. Its main purpose is to display [QuarkSuite
Core](https://codeberg.org/quarksuite/core) generated data, but you can use it standalone or with equivalent data.

This is an initial release, so it's a proof of concept rather than a production-ready library. Some things may break or
not work as well as they could work.

## Packages

-   [Nest.land](https://nest.land): `https://x.nest.land/quarksuite:elements@0.6.0/[element]`

This package is only available for use via Nest.land as it's **highly experimental** at this stage.

## Getting Started

You can add the following `<script>` tags as needed into an HTML file in either the head or body to load the elements.

```html
<script type="module" src="https://x.nest.land/quarksuite:elements@0.6.0/color-token.js"></script>
<script type="module" src="https://x.nest.land/quarksuite:elements@0.6.0/text-token.js"></script>
<script type="module" src="https://x.nest.land/quarksuite:elements@0.6.0/unit-token.js"></script>
<script type="module" src="https://x.nest.land/quarksuite:elements@0.6.0/grid-token.js"></script>
```

Or you can load them all at once by sourcing `mod.js`.

```html
<script type="module" src="https://x.nest.land/quarksuite:elements@0.6.0/mod.js"></script>
```

## Elements

The components are named by the kind of token they render. All elements have shadow parts that correspond with generated
data. This makes it easy to hide and highlight values as desired.

### \<color-token>

Renders a color and desired formats.

#### Properties/Attributes

-   `swatch = "#808080": string`: input color (any valid CSS color)
-   `formats = "hex rgb hsl": "hex" | "rgb" | "hsl" | "cmyk" | "hwb" | "lab" | "lch" | "oklab" | "oklch"`: desired CSS
    color formats (space separated)

#### Customization

##### Custom Properties
    
-   `--spacing`: set element spacing (default is `1ex`)
-   `--swatch-size`: set color swatch size (default is `10vh`)
-   `--data-size`: set data text size (default is `1rem`)
    
##### Shadow Parts
    
-   `::part(swatch)`: style color swatch
-   `::part(data)`: style color data formats
-   `::part(value)`: style color format values
-   `::part(actual)`: style color format value of actual input color

#### Examples

##### Markup
    
```html
<color-token swatch="#3ccaaf" format="rgb hex"></color-token>
```
    
##### DOM
    
```js
const el = document.createElement("color-token");
el.swatch = "hsl(130, 57%, 64%);
el.format = "hsl rgb hex";

document.body.append(el);
```

### \<text-token>

Renders a text sample with its typographical qualities.

#### Properties/Attributes

-   `stack = "sans-serif": string`: sample font stack
-   `size = "1.5rem": string`: sample font size
-   `weight = "regular": "thin" | "extralight" | "light" | "regular" | "medium" | "semibold" | "bold" | "extrabold" |
    "black"`: sample font weight
-   `leading = 1.5: number`: sample leading (line height)
-   `measure = "75ch": string`: sample measure (chars per line)

#### Customization

##### Custom Properties
    
-   `--spacing`: set element spacing (default is `1ex`)
-   `--data-size`: set data text size (default is `1rem`)
    
##### Shadow Parts
    
-   `::part(content)`: style text content
-   `::part(data)`: style text data
-   `::part(font)`: style font data
-   `::part(typography)`: style typography data
-   `::part(value)`: style text data value

There are also parts corresponding to each property to make it easier to highlight or hide data without JavaScript.

#### Examples

##### Markup
    
```html
<text-token
  stack="Titillium Web, sans-serif"
  size="1.25rem"
  weight="regular"
  leading="1.25"
  measure="45ch"></text-token>
```
    
##### DOM
    
 ```js
 const el = document.createElement("text-token");
 el.stack = "Mozilla Slab, serif";
 el.size = "1.5rem";
 el.weight = "bold";
 el.leading = 1.414;
 el.measure = "64ch";

 document.body.append(el);
 ```

### \<unit-token>

Renders a unit sample from a valid CSS value.

#### Properties/Attributes

-   `length = "960px": string`: the length to render
-   `grid = false: boolean`: set grid mode (for `fr` values)

#### Customization

##### Custom Properties

-   `--spacing`: set element spacing (default is `1ex`)
-   `--element-gap`: set element gap (default is `var(--spacing)`)
-   `--element-height`: set element height (default is `2vmin`)
-   `--element-color`: set element color (default is `currentcolor`)
-   `--length-color`: set length segment color (default is `gray`)
-   `--data-size`: set data text size (default is `1rem`)

##### Shadow Parts

-   `::part(element)`: style element sample
-   `::part(data)`: style unit data
-   `::part(value)`: style unit data value

#### Examples

##### Markup

```html
<unit-token length="75vw"></unit-token>
<unit-token length="2.5fr" grid></unit-token>
```

##### DOM

```js
const el = document.createElement("unit-token");
el.length = "30%";
// el.grid = true;

document.body.append(el);
```

### \<grid-token>

Renders a grid sample from columns and rows.

#### Properties/Attributes

-   `columns = 8: number`: number of grid columns
-   `rows = columns: number`: number of grid rows

#### Customization

##### Custom Properties

-   `--spacing`: set element spacing (default is `1ex`)
-   `--cell-gap`: set gap between grid cells (default is `var(--spacing)`)
-   `--cell-color`: set cell color (default is `currentcolor`)
-   `--cell-width`: set cell width (default is `8vmin`)
-   `--cell-height`: set cell height (default is `calc(var(--cell-width) / 2)`)
-   `--data-size`: set data text size (default is `1rem`)

##### Shadow Parts

-   `::part(grid)`: style grid container
-   `::part(cell)`: style grid cells
-   `::part(data)`: style grid data
-   `::part(value)`: style grid data value

As with the other tokens, a part is exposed for each property to hide or highlight data without JavaScript.

#### Examples

##### Markup

```html
<grid-token columns="6" rows="4"></grid-token>
```

##### DOM

```js
const el = document.createElement("grid-token");
el.columns = 10;
// el.rows = 7;

document.body.append(el);
```
