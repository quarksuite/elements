// If you use QuarkSuite Elements offline, don't forget to grab this file!

/**
 * The QSElement superclass contains all shared functionality between elements.
 * It manages attaching the Shadow DOM, and live attribute updates.
 */
export class QSElement extends HTMLElement {
  /**
   * The constructor() sets up defaults and attaches the shadow DOM.
   */
  constructor() {
    super();

    this.attachShadow({ mode: "open" });
  }

  /**
   * Here, connectedCallback() is responsible for populating the shadow root with
   * a rendered template.
   */
  connectedCallback() {
    globalThis.addEventListener("DOMContentLoaded", () => {
      this.shadowRoot.appendChild(this.render());
    });
  }

  /**
   * attributeChangedCallback() listens for any modifications on observed
   * attributes and refreshes the element.
   */
  attributeChangedCallback(_, value) {
    this.update(value);
  }

  /**
   * update() will only fire if any observed attributes have actually changed.
   */
  update(value) {
    if (value !== null) {
      this.shadowRoot.replaceChildren(this.render());
    }
  }

  /**
   * Finally, reflect() guarantees that any elements created through the DOM will
   * have their equivalent attributes set.
   */
  reflect(name, value) {
    if (value || value === "") {
      this.setAttribute(name, value);
    } else {
      this.removeAttribute(name);
    }
  }
}
