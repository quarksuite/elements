import { QSElement } from "./lib.js";

export class GridToken extends QSElement {
  constructor() {
    super();
  }

  render() {
    const columns = this.columns || 8;
    const rows = this.rows || columns;

    return template(columns, rows);
  }

  static get observedAttributes() {
    return ["columns", "rows"];
  }

  set columns(value) {
    this.reflect("columns", value);
  }

  get columns() {
    return this.getAttribute("columns");
  }

  set rows(value) {
    this.reflect("rows", value);
  }

  get rows() {
    return this.getAttribute("rows");
  }
}

if (!customElements.get("grid-token")) {
  customElements.define("grid-token", GridToken);
}

function template(columns, rows) {
  const tmpl = document.createElement("template");

  tmpl.innerHTML = `
${styles(columns, rows)}
<div part="grid">
${Array(columns * rows)
  .fill(`<div part="cell"></div>`)
  .join("\n")}
</div>
<div part="data">
  <span part="value columns">columns: <code part="code">${columns}</code></span>
  <span part="value rows">rows: <code part="code">${rows}</code></span>
</div>
`;

  return tmpl.content.cloneNode(true);
}

function styles(columns, rows) {
  return `
<style>
  :host {
    --spacing: 1ex;
    --cell-gap: var(--spacing);
    --cell-color: currentcolor;
    --cell-width: 8vmin;
    --cell-height: calc(var(--cell-width) / 2);
    --data-size: 1rem;

    display: block;
  }

  :host[hidden] {
    display: none;
  }

  [part="grid"] {
    display: grid;
    grid-template-columns: repeat(${columns}, minmax(var(--cell-width), 1fr));
    grid-template-rows: repeat(${rows}, minmax(var(--cell-height), 1fr));
    gap: var(--cell-gap);
  }

  [part="cell"] {
    background-color: var(--cell-color);
  }

  [part="data"] {
    font-size: var(--data-size);
    margin-top: var(--spacing);
  }

  [part~="value"] {
    display: block;
    margin-bottom: calc(var(--spacing) / 4);
  }
</style>
`;
}
