import { convert } from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/color.js";
import { QSElement } from "./lib.js";

export class ColorToken extends QSElement {
  constructor() {
    super();
  }

  render() {
    const swatch = this.swatch || "#808080";
    const format = this.format || "hex rgb hsl";

    return template(swatch, format);
  }

  static get observedAttributes() {
    return ["swatch", "format"];
  }

  set swatch(value) {
    this.reflect("swatch", value);
  }

  get swatch() {
    return this.getAttribute("swatch");
  }

  set format(value) {
    this.reflect("format", value);
  }

  get format() {
    return this.getAttribute("format");
  }
}

if (!customElements.get("color-token")) {
  customElements.define("color-token", ColorToken);
}

// Formatting
function formats(swatch, format) {
  return format
    .split(" ")
    .map((format) => {
      return `<span part="value ${format}">${format}: <code part="code ${
        swatch === convert(format, swatch) && `actual`
      }">${convert(format, swatch)}</code></span>`;
    })
    .join("");
}

// Template
function template(swatch, format) {
  const tmpl = document.createElement("template");

  tmpl.innerHTML = `
${styles(swatch)}
<div part="swatch"></div>
<div part="data">
${formats(swatch, format)}
</div>
`;

  return tmpl.content.cloneNode(true);
}

// Styles
function styles(swatch) {
  return `
<style>
  :host {
    --spacing: 1ex;
    --swatch-size: 10vh;
    --data-size: 1rem;

    display: block;
  }

  :host[hidden] {
    display: none;
  }

  [part="swatch"] {
    background: ${convert("hex", swatch)};
    min-width: var(--swatch-size);
    min-height: var(--swatch-size);
  }

  [part="data"] {
    font-size: var(--data-size);
    margin-top: var(--spacing);
  }

  [part~="value"] {
    display: block;
    margin-bottom: calc(var(--spacing) / 4);
  }

  [part~="actual"] {
    font-weight: 700;
  }
</style>
`;
}
