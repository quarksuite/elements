import { QSElement } from "./lib.js";

export class TextToken extends QSElement {
  constructor() {
    super();
  }

  render() {
    const stack = this.stack || "sans-serif";
    const size = this.size || "1rem";
    const weight = this.weight || "regular";
    const leading = this.leading || 1.5;
    const measure = this.measure || "75ch";

    return template(stack, size, weight, leading, measure);
  }

  static get observedAttributes() {
    return ["stack", "size", "weight", "leading", "measure"];
  }

  set stack(value) {
    this.reflect("stack", value);
  }

  get stack() {
    return this.getAttribute("stack");
  }

  set size(value) {
    this.reflect("size", value);
  }

  get size() {
    return this.getAttribute("size");
  }

  set weight(value) {
    this.reflect("weight", value);
  }

  get weight() {
    return this.getAttribute("weight");
  }

  set leading(value) {
    this.reflect("leading", value);
  }

  get leading() {
    return this.getAttribute("leading");
  }

  set measure(value) {
    this.reflect("measure", value);
  }

  get measure() {
    return this.getAttribute("measure");
  }
}

if (!customElements.get("text-token")) {
  customElements.define("text-token", TextToken);
}

function fontWeight(style) {
  return new Map([
    ["thin", 100],
    ["extralight", 200],
    ["light", 300],
    ["regular", 400],
    ["medium", 500],
    ["semibold", 600],
    ["bold", 700],
    ["extrabold", 800],
    ["black", 900],
  ]).get(style);
}

function template(stack, size, weight, leading, measure) {
  weight = fontWeight(weight);
  const tmpl = document.createElement("template");

  tmpl.innerHTML = `
${styles(stack, size, weight, leading, measure)}
<div part="content">A quart jar of oil mixed with zinc oxide makes a very bright paint.</div>
<div part="data">
  <div part="font">
    <span part="value stack">stack: <code part="code">"${stack}"</code></span>
    <span part="value size">size: <code part="code">"${size}"</code></span>
    <span part="value weight">weight: <code part="code">${weight}</code></span>
  </div>
  <div part="typography">
    <span part="value leading">leading: <code part="code">${leading}</code></span>
    <span part="value measure">measure: <code part="code">"${measure}"</code></span>
  </div>
</div>
`;

  return tmpl.content.cloneNode(true);
}

function styles(font, size, weight, leading, measure) {
  return `
<style>
  :host {
    --spacing: 1ex;
    --data-size: 1rem;

    display: block;
  }

  :host[hidden] {
    display: none;
  }

  [part="content"] {
    font-family: ${font};
    font-size: ${size};
    font-weight: ${weight};
    line-height: ${leading};
    max-width: ${measure};
  }

  [part="data"] {
    font-size: var(--data-size);
    margin-top: var(--spacing);
  }

  [part="font"], [part="typography"] {
    margin-top: var(--spacing);
  }

  [part~="value"] {
    display: block;
    margin-bottom: calc(var(--spacing) / 4);
  }
</style>
`;
}
